# Bugzilla milestones

Quick and dirty python script using requests module to login and create milestones with Bugzilla CGI scripts.

Couldn't find any information about creating milestones in Bugzilla Rest API documentation, so here is a basic code you
could reuse and improve to create milestones and more.

Tested on Bugzilla 5.0.6.
