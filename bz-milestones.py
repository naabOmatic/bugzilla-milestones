#!/usr/bin/env python

"""Create a new milestone using Bugzilla CGI script"""

import re
import requests


# Vars
url_base = 'https://bugzilla.local'
url_home = url_base + '/bugzilla/index.cgi'
url_editmilestones = url_base + '/bugzilla/editmilestones.cgi'
username = 'root@localhost.local'
password = 'password'
product = 'TestProduct'  # Name of your Bugzilla product where you want to add a new milestone
target_milestone = '1.0'

# Create new requests session object
with requests.Session() as s:
    # Disable SSL verify
    s.verify = False

    # Make a first connection then a second one which will generate the 'Bugzilla_login_token'
    s.get(url_home)
    r = s.get(url_home)
    pattern = re.compile(r'name=\"Bugzilla_login_token\"\s+value=\"(.*)\"', re.MULTILINE)
    blt = re.findall(pattern, r.text)[0]

    # Login to Bugzilla
    payload = {
        'Bugzilla_login': username,
        'Bugzilla_password': password,
        'Bugzilla_login_token': blt,
        'GoAheadAndLogIn': 'Log+in',
    }
    r = s.post(url_home, data=payload)

    # Get token
    payload = {
        "action": "add",
        "product": product
    }
    r = s.get(url_editmilestones, params=payload)
    pattern = re.compile(r'name=\"token\" value=\"(.*)\"')
    for line in r.text.splitlines():
        t = re.search(pattern, line)
        if t:
            token = t.group(1)
            break

    # Create milestone
    payload = {
        'milestone': target_milestone,
        'sortkey': '',
        'action': 'new',
        'product': product,
        'token': token
    }
    s.post(url_editmilestones, params=payload)
